const NodeCache = require('node-cache')

const config = {
    prefix: '/',
    openaikey: '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',
    cache: new NodeCache({
        checkperiod: 10000,
        deleteOnExpire: true
    }),
    ratelimit: new Map(),
    commands: new Map(),
    aliases: new Map()
}

module.exports = config