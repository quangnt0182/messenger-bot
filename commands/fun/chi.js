const { Client, Message } = require('messenger-api.js')
const config = require('../../config/config.js')
const user_info = require('../../config/user.js')
const fs = require('fs');

const ms = require('ms')

const axios = require('axios');

module.exports = {
    name: 'chi',
    category: 'fun',
    aliases: ['askgpt'],
    /**
     * @arg {Client<true>} client 
     * @arg {Message} message
     * @arg {string[]} args
    */
    run: async (client, message, args) => {
        if (!args.length) return message.reply(`❎ Hãy nhập tin nhắn bạn muốn hỏi.`)
        if (config.ratelimit.has(message.thread.id))
            return message.thread.send('❎ Bot đang xử lý câu hỏi trước đó, xin vui lòng đợi.')
        


        const msg = args.join(' ')


        console.log(args);

        config.ratelimit.set(message.thread.id, true)

        content_reply = 'Hi';

        // message.reply('Processing Chi...', { typing: true })

        // check exist file name message.thread.id


        if (fs.existsSync('./data/chi/' + message.thread.id + '.json')) {
            console.log('File exist.');
            let rawdata = fs.readFileSync('./data/chi/' + message.thread.id + '.json');
            let data = JSON.parse(rawdata);

            switch (data.step) {
                case "cost":
                    data.cost = msg;
                    // regex msg is money
                    if (msg.match(/^[0-9]+$/)) {
                        data.cost = msg;
                        data.step = "name"
                        content_reply = 'Ok, đã chi ' + msg + ' rồi thì chi cho ai?';
                    } else { 
                        content_reply = 'Nhập số thôi bạn ơi';
                        data.cost = 0;
                    }
                    break;
                case "name":
                    data.address = msg;

                    // regex msg is name
                    if (msg.match(/^[a-zA-Z]+$/)) {
                        data.name = msg;
                        
                        content_reply = 'Ok, chi cho ' + msg + ' rồi còn ai nữa không? Nếu không thì nhập "không"';
                    } else { 
                        content_reply = 'Nhập tên thôi bạn ơi';
                        data.name = '';
                    }


                    data.step = "phone"
                    break;
                case "không":
                    data.phone = msg;

                    content_reply = 'Tạm biệt bạn, hẹn gặp lại';
                    data.step = "done"
                    break;
                case "done":
                    data.done = msg;
                    data.step = "cost"
                    break;
                default:
                    data.step = "cost"
                    break;
            }

            data.prompt = msg;
            let jsonData = JSON.stringify(data)
            fs.writeFileSync('./data/chi/' + message.thread.id + '.json', jsonData)
        } else {
            console.log('File not exist.');
            let data = {
                "prompt": msg,
                "step": "cost"
            }
            let fileName = message.thread.id + '.json'
            let jsonData = JSON.stringify(data)
            fs.writeFileSync('./data/chi/' + fileName, jsonData)

            content_reply = 'Chi ' + msg +' hết nhiu tiền?';
        }


        message.reply(content_reply, { typing: true })
        config.ratelimit.delete(message.thread.id)
        
        // const history = config.cache.get(message.thread.id)
        
        // await message.thread.messages.markAsRead()
        // await api.createChatCompletion({
        //     model: 'gpt-3.5-turbo',
        //     messages: !history ? [
        //         { role: 'user', content: msg }
        //     ] : [
        //         { role: 'user', content: history.question },
        //         { role: 'assistant', content: history.answer },
        //         { role: 'user', content: msg }
        //     ],
        //     max_tokens: 2048
        // }).then(res => {
        //     const success = res.data.choices[0].message.content
        //     config.cache.del(message.thread.id)

        //     config.cache.set(message.thread.id, { question: msg, answer: success }, ms('5m'))
        //     config.ratelimit.delete(message.thread.id)

        //     return message.reply(success, { typing: true })
        // }).catch(error => {
        //     console.error(error)
        //     config.ratelimit.delete(message.thread.id)
        // })
    }
}
