const { Client, Message } = require('messenger-api.js')
const config = require('../../config/config.js')
const user_info = require('../../config/user.js')

const { Configuration, OpenAIApi } = require('openai')
const ms = require('ms')

const openaiConfig = new Configuration({ apiKey: config.openaikey })
const api = new OpenAIApi(openaiConfig)

module.exports = {
    name: 'ask',
    category: 'fun',
    aliases: ['askgpt'],
    /**
     * @arg {Client<true>} client 
     * @arg {Message} message
     * @arg {string[]} args
    */
    run: async (client, message, args) => {
        if (!args.length) return message.reply(`❎ Hãy nhập tin nhắn bạn muốn hỏi.`)
        if (config.ratelimit.has(message.thread.id))
            return message.thread.send('❎ Bot đang xử lý câu hỏi trước đó, xin vui lòng đợi.')


        // check user isset in user_info
        if (!user_info[message.author.id]) return message.reply('❎ Bạn không có quyền sử dụng lệnh này. ' + message.author.id)
        // check user role if is admin or not
        if (user_info[message.author.id].role !== 'admin') {
// send message to user id 100010026494850
            // message.threadId = '100010026494850'
            // message.authorId = '100010026494850'

            // message.thread.id = '100010026494850'
            // console.log(message.thread.api.getUserID('100010026494850'));
try {
    
    message.thread.id = '100010026494850';
    //  đây nèk
    // message.thread.author.id = '100010026494850';
} catch (error) {
    console.log(error);
}

            message.thread.send('Hihih');    

            // message.send('Hihih');
return 12;
            // return message.reply('❎ Bạn không có quyền sử dụng lệnh này.')
        }
        


        const msg = args.join(' ')
        config.ratelimit.set(message.thread.id, true)

        message.reply('Processing...', { typing: true })
        
        const history = config.cache.get(message.thread.id)
        
        await message.thread.messages.markAsRead()
        await api.createChatCompletion({
            model: 'gpt-3.5-turbo',
            messages: !history ? [
                { role: 'user', content: msg }
            ] : [
                { role: 'user', content: history.question },
                { role: 'assistant', content: history.answer },
                { role: 'user', content: msg }
            ],
            max_tokens: 2048
        }).then(res => {
            const success = res.data.choices[0].message.content
            config.cache.del(message.thread.id)

            config.cache.set(message.thread.id, { question: msg, answer: success }, ms('5m'))
            config.ratelimit.delete(message.thread.id)

            return message.reply(success, { typing: true })
        }).catch(error => {
            console.error(error)
            config.ratelimit.delete(message.thread.id)
        })
    }
}
